<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Stellar Inventory QR</title>
    <meta name="viewport" content="width=device-width">
    <style>
      .button {
          background-color: #e6e6ff;
          border: none;
          color: white;
          padding: 90px 128px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 64px;
          color: #000000;
          margin: 4px 2px;
          cursor: pointer;
      }

      .center{
        margin: auto;
        text-align: center;
        width: 60%;
        border: 2px solid #00001a;
        padding: 10px;
      }

      .headerr{
        margin: auto;
        text-align: center;
        width: 60%;
      }
    </style>
</head>
<body>
  <div class="headerr">
    <h1>Stellar Team QR Tracker</h1>
  </div>
  <div class="center">
    <a href="QR_Add.php" class="button">Add</a>
    <a href="QR_List.php" class="button">View</a>
  </div>
</body>
</html>
