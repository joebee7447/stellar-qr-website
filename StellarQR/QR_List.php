<html>
  <head>
    <style>
      .button {
          background-color: #e6e6ff;
          border: none;
          color: white;
          padding: 50px 100px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 32px;
          color: #000000;
          margin: 4px 2px;
          cursor: pointer;
      }

      #myInput {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 20%;
        height: 5%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #IDmyInput {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 20%;
        height: 5%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
      }

    </style>

  </head>
  <body>
    <h1><a href="index.php" class="button">Back to Home Page</a></h1>
    <input type="text" id="IDmyInput" onkeyup="IDsearchFunction()" placeholder="Search for ID..">
    <input type="text" id="myInput" onkeyup="searchFunction()" placeholder="Search..">
    <table id = "myTable" border='1'>
      <tr>
        <th style="width:10%;" + onclick="sortTable(0)">ID</th>
        <th style="width:10%;" + onclick="sortTable(1)">Item</th>
        <th style="width:10%;"+ onclick="sortTable(2)">Previous Location</th>
        <th style="width:10%;" + onclick="sortTable(3)">Current Location</th>
        <th style="width:10%;" + onclick="sortTable(4)">State</th>
        <th style="width:10%;" + onclick="sortTable(5)">Last Scan</th>
        <th style="width:10%;" + onclick="sortTable(6)">QR Code</th>
    <?
      //Gets connection to database
      $serverName = "stellerqr.database.windows.net";
      $connectionOptions = array(
          "Database" => "StellarQR-Inventory",
          "Uid" => "StellerTeam",
          "PWD" => "Pullrequest1"
      );
      //Establishes the connection
      $conn = sqlsrv_connect($serverName, $connectionOptions);
      $tsql= "SELECT ID, Item, Prev_Loc, Current_Loc, State, Last_Scan FROM dbo.StellarQR_Table";
      $getResults= sqlsrv_query($conn, $tsql);
      echo "<br>";
      if ($getResults == FALSE)
          echo (sqlsrv_errors());
      //Prints out each item from the database
      while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {

        echo "<tr>";
        echo "<td>".$row['ID']."</td>";
        echo "<td>".$row['Item']."</td>";
        echo "<td>".$row['Prev_Loc']."</td>";
        echo "<td>".$row['Current_Loc']."</td>";
        echo "<td>".$row['State']."</td>";
        echo "<td>".date_format($row['Last_Scan'],"Y-m-d H:i:s")."</td>";
        echo "<td>" . " " ." <a " . "href=" . "\"" . "\"". "target=". "\""
        ."_blank". "\"".">"."QR Code"."</a>" . "</td>";
        echo "</tr>";
      }
      sqlsrv_free_stmt($getResults);
    ?>
    <script>
    function IDsearchFunction() {
      var IDinput, IDfilter, IDtable, IDtr, IDtd, IDi;
      IDinput = document.getElementById("IDmyInput");
      IDfilter = IDinput.value.toUpperCase();
      IDtable = document.getElementById("myTable");
      IDtr = IDtable.getElementsByTagName("tr");
      for (IDi = 0; IDi < IDtr.length; IDi++) {
        IDtd = IDtr[IDi].getElementsByTagName("td")[0];
        if (IDtd) {
          if (IDtd.innerHTML.toUpperCase().indexOf(IDfilter) > -1) {
            IDtr[IDi].style.display = "";
          }else {
            IDtr[IDi].style.display = "none";
          }
      }
    }
  }

    function searchFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td1 = tr[i].getElementsByTagName("td")[1];
        td2 = tr[i].getElementsByTagName("td")[2];
        td3 = tr[i].getElementsByTagName("td")[3];
        td4 = tr[i].getElementsByTagName("td")[4];
        td5 = tr[i].getElementsByTagName("td")[5];
        td6 = tr[i].getElementsByTagName("td")[5];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.Display = "";
          } else if (td2.innerHTML.toUpperCase().indexOf(filter) > -1){
            tr[i].style.Display = "";
          } else if (td3.innerHTML.toUpperCase().indexOf(filter) > -1){
            tr[i].style.Display = "";
          } else if (td4.innerHTML.toUpperCase().indexOf(filter) > -1){
            tr[i].style.Display = "";
          } else if (td5.innerHTML.toUpperCase().indexOf(filter) > -1){
            tr[i].style.Display = "";
          } else if (td6.innerHTML.toUpperCase().indexOf(filter) > -1){
            tr[i].style.Display = "";
          }else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    function sortTable(n) {
      var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
      table = document.getElementById("myTable");
      switching = true;
      dir = "asc";
      while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 1; i < (rows.length - 1); i++) {
          shouldSwitch = false;
          x = rows[i].getElementsByTagName("TD")[n];
          y = rows[i + 1].getElementsByTagName("TD")[n];
          if (dir == "asc") {
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
              shouldSwitch= true;
              break;
            }
          } else if (dir == "desc") {
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
              shouldSwitch= true;
              break;
            }
          }
        }
        if (shouldSwitch) {
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
          switchcount ++;
        } else {
          if (switchcount == 0 && dir == "asc") {
            dir = "desc";
            switching = true;
          }
        }
      }
    }
    </script>
  </body>
</html>
