<!doctype html>
<html lang="en">
<head>
  <style>
    .button {
        background-color: #e6e6ff;
        border: none;
        color: white;
        padding: 50px 100px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 32px;
        color: #000000;
        margin: auto;
        cursor: pointer;
    }

    .error{
      color: #FF0000;
    }

    .center{
      margin: auto;
      text-align: center;
      width: 60%;
      border: 2px solid #00001a;
      padding: 10px;
    }

    .theForm{
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }

  </style>
</head>
<body>
<div class="center">
<h1><a href="index.php" class="button">Back to Home Page</a></h1>
<?
include('phpqrcode/qrlib.php');
$item = $prev_Loc = $current_loc = $state = "";
$itemERR = $current_locERR = $stateERR = "";
$check = 0;
if($_SERVER["REQUEST_METHOD"] == "POST"){
  if(empty($_POST["qrItem"])){
    $itemERR = "Item is Required";
  }else{
    $item = test_input($_POST["qrItem"]);
    $check++;
  }
  if(empty($_POST["qrCurrentLoc"])){
    $current_locERR = "Current Location is Required";
  }else{
    $current_loc = test_input($_POST["qrCurrentLoc"]);
    $check++;
  }
  $state = test_input($_POST["qrState"]);
}

//verifies that the information inputed is safe
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

//When the submit button is pressed
if(isset($_POST['submit']))
{
  if($check > 1){
    try {
        //connects to database to write to it
        $conn = new PDO("sqlsrv:server = tcp:stellerqr.database.windows.net,1433; Database = StellarQR-Inventory", "StellerTeam", "Pullrequest1");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $datetime = date("Y-m-d H:i:s");

        //connects to the database for the unique ID
        $serverName = "stellerqr.database.windows.net";
        $connectionOptions = array(
            "Database" => "StellarQR-Inventory",
            "Uid" => "StellerTeam",
            "PWD" => "Pullrequest1"
        );
        //Establishes the connection
        $connRead = sqlsrv_connect($serverName, $connectionOptions);

        //Gets the ID's from the table making a unique one
        $result = "SELECT ID FROM dbo.StellarQR_Table";
        $getResults = sqlsrv_query($connRead, $result);
        $id = 0;
        //gets a unique ID for the next item to be added
        while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
          $temp = $row['ID'];
          if($id == 0){
            $id = $temp;
          }
          $id = $id + 1;
        }

        //inserts the new item and its info in the table
        $sql = "INSERT INTO dbo.StellarQR_Table
        (ID, Item, Prev_Loc, Current_Loc, State, Last_Scan)
        VALUES ($id, '$item', '$prev_loc','$current_loc','$state','$datetime')";
        $conn->exec($sql);
        echo "<br>"."New record created successfully"."<br>";
        // echo "<a " . "href="."\"" . QRcode::png($id) . "\"".
        // " target=" ."\"_blank\">". "QR Code HERE" . "</a>";
    }
    catch (PDOException $e) {
        print("Error connecting to SQL Server.");
        die(print_r($e));
    }
    $conn = null;
  }
}
?>
<div class="center">
<p><span class="error">* required field</span></p>
<form class="theForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    Item:  <input type="text" name="qrItem"><span class="error">*<?php echo $itemERR;?></span><br>
    Current Location:   <input type="text" name="qrCurrentLoc"><span class="error">*<?php echo $current_locERR;?></span><br>
    <!--State: <input type="text" name="qrState"><span class="error">*<?php //echo $stateERR;?></span><br>-->
    State: <select name="qrState">
      <option value="Load In">Load In</option>
      <option value="Load Out">Load Out</option>
      <option value="Storage">Storage</option>
    </select><br>
    Submit: <input type="submit" value="Add" name="submit">
</form>
</div>
</body>
</html>
